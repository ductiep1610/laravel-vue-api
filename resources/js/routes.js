// Defind router to component
import Vue from "vue";
import VueRouter from "vue-router";
import Home from "./pages/Home.vue";
import Login from "./pages/Login.vue";
import Register from "./pages/Register.vue";
import Todo from "./components/Todo.vue";
import DashBoard from "./pages/DashBoard.vue";

Vue.use(VueRouter);

export const routes = [
    {
        path: "/",
        name: "Home",
        component: Home,
    },
    {
        path: "/login",
        name: "Login",
        component: Login,
        meta: { guest: true },
    },
    {
        path: "/register",
        name: "Register",
        component: Register,
        meta: { guest: true },
    },
    {
        path: "/dashboard",
        name: "DashBoard",
        component: DashBoard,
        meta: { requireAuth: true },
    },
    // {
    //     path: '/api/todos',
    //     name: 'TodoList',
    //     component: Todo
    // },
];
