require("./bootstrap");
import Vue from "vue";
import App from "./layouts/App.vue";
import axios from "axios";
import VueAxios from "vue-axios";
import VueRouter from "vue-router";
import { routes } from "./routes";
import TodoList from "./components/Todo.vue";
import { store } from "./store/store.js";

Vue.use(VueAxios, axios);

axios.defaults.withCredentials = true;
axios.defaults.baseURL = "http://127.0.0.1:8000/api/";
const token = localStorage.getItem("token");

if (token) {
    axios.defaults.headers.common["Authorization"] = token;
}

// manage error and expire token
axios.interceptors.response.use(undefined, function (error) {
    if (error) {
        const originalRequest = error.config;
        if (error.response.status === 401 && !originalRequest._retry) {
            originalRequest._retry = true;
            store.dispatch("logout");
            return router.push("/login");
        } else {
            store.commit("handle_error", JSON.parse(error.response.data.error));
        }
    }
});

//Đăng ký route
const router = new VueRouter({
    routes: routes,
    mode: "history",
});

router.beforeEach((to, from, next) => {
    if (to.matched.some((record) => record.meta.requireAuth)) {
        if (store.getters.isLoggedIn) {
            next();
            return;
        }
        next("/login");
    } else {
        next();
    }
});

router.beforeEach((to, from, next) => {
    if (to.matched.some((record) => record.meta.guest)) {
        if (store.getters.isLoggedIn) {
            next("/dashboard");
            return;
        }
        next();
    } else {
        next();
    }
});

const app = new Vue({
    el: "#app",
    router: router,
    store: store,
    render: (app) => app(App),
});
